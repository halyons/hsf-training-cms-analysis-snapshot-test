#Start from the rootproject root conda image
FROM rootproject/root-conda:6.18.04

COPY . /analysis/skim

WORKDIR /analysis/skim

RUN echo ">>> Compiling skimming executable ... " && \
    COMPILER=$(root-config --cxx) && \
    FLAGS=$(root-config --cflags --libs) && \
    $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o skim skim.cxx $FLAGS